defmodule AsnaValidationWeb.RequirementControllerTest do
  use AsnaValidationWeb.ConnCase

  alias AsnaValidation.Templates
  alias AsnaValidation.Templates.Requirement

  @create_attrs %{model: %{}, type: "some type"}
  @update_attrs %{model: %{}, type: "some updated type"}
  @invalid_attrs %{model: nil, type: nil}

  def fixture(:requirement) do
    {:ok, requirement} = Templates.create_requirement(@create_attrs)
    requirement
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all requirements", %{conn: conn} do
      conn = get conn, requirement_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  # describe "create requirement" do
  #   test "renders requirement when data is valid", %{conn: conn} do
  #     conn = post conn, requirement_path(conn, :create), requirement: @create_attrs
  #     assert %{"id" => id} = json_response(conn, 201)["data"]

  #     conn = get conn, requirement_path(conn, :show, id)
  #     assert json_response(conn, 200)["data"] == %{
  #       "id" => id,
  #       "model" => %{},
  #       "type" => "some type"}
  #   end

  #   test "renders errors when data is invalid", %{conn: conn} do
  #     conn = post conn, requirement_path(conn, :create), requirement: @invalid_attrs
  #     assert json_response(conn, 422)["errors"] != %{}
  #   end
  # end

  # describe "update requirement" do
  #   setup [:create_requirement]

  #   test "renders requirement when data is valid", %{conn: conn, requirement: %Requirement{id: id} = requirement} do
  #     conn = put conn, requirement_path(conn, :update, requirement), requirement: @update_attrs
  #     assert %{"id" => ^id} = json_response(conn, 200)["data"]

  #     conn = get conn, requirement_path(conn, :show, id)
  #     assert json_response(conn, 200)["data"] == %{
  #       "id" => id,
  #       "model" => %{},
  #       "type" => "some updated type"}
  #   end

  #   test "renders errors when data is invalid", %{conn: conn, requirement: requirement} do
  #     conn = put conn, requirement_path(conn, :update, requirement), requirement: @invalid_attrs
  #     assert json_response(conn, 422)["errors"] != %{}
  #   end
  # end

  # describe "delete requirement" do
  #   setup [:create_requirement]

  #   test "deletes chosen requirement", %{conn: conn, requirement: requirement} do
  #     conn = delete conn, requirement_path(conn, :delete, requirement)
  #     assert response(conn, 204)
  #     assert_error_sent 404, fn ->
  #       get conn, requirement_path(conn, :show, requirement)
  #     end
  #   end
  # end

  defp create_requirement(_) do
    requirement = fixture(:requirement)
    {:ok, requirement: requirement}
  end
end
