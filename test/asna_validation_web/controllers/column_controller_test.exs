defmodule AsnaValidationWeb.ColumnControllerTest do
  use AsnaValidationWeb.ConnCase

  alias AsnaValidation.Templates
  alias AsnaValidation.Templates.Column

  @create_attrs %{active: true, name: "some name", optional: true}
  @update_attrs %{active: false, name: "some updated name", optional: false}
  @invalid_attrs %{active: nil, name: nil, optional: nil}

  def fixture(:column) do
    {:ok, column} = Templates.create_column(@create_attrs)
    column
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all columns", %{conn: conn} do
      conn = get conn, column_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  # describe "create column" do
  #   test "renders column when data is valid", %{conn: conn} do
  #     conn = post conn, column_path(conn, :create), column: @create_attrs
  #     assert %{"id" => id} = json_response(conn, 201)["data"]

  #     conn = get conn, column_path(conn, :show, id)
  #     assert json_response(conn, 200)["data"] == %{
  #       "id" => id,
  #       "active" => true,
  #       "name" => "some name",
  #       "optional" => true}
  #   end

  #   test "renders errors when data is invalid", %{conn: conn} do
  #     conn = post conn, column_path(conn, :create), column: @invalid_attrs
  #     assert json_response(conn, 422)["errors"] != %{}
  #   end
  # end

  # describe "update column" do
  #   setup [:create_column]

  #   test "renders column when data is valid", %{conn: conn, column: %Column{id: id} = column} do
  #     conn = put conn, column_path(conn, :update, column), column: @update_attrs
  #     assert %{"id" => ^id} = json_response(conn, 200)["data"]

  #     conn = get conn, column_path(conn, :show, id)
  #     assert json_response(conn, 200)["data"] == %{
  #       "id" => id,
  #       "active" => false,
  #       "name" => "some updated name",
  #       "optional" => false}
  #   end

  #   test "renders errors when data is invalid", %{conn: conn, column: column} do
  #     conn = put conn, column_path(conn, :update, column), column: @invalid_attrs
  #     assert json_response(conn, 422)["errors"] != %{}
  #   end
  # end

  # describe "delete column" do
  #   setup [:create_column]

  #   test "deletes chosen column", %{conn: conn, column: column} do
  #     conn = delete conn, column_path(conn, :delete, column)
  #     assert response(conn, 204)
  #     assert_error_sent 404, fn ->
  #       get conn, column_path(conn, :show, column)
  #     end
  #   end
  # end

  defp create_column(_) do
    column = fixture(:column)
    {:ok, column: column}
  end
end
