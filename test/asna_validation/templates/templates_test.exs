defmodule AsnaValidation.TemplatesTest do
  use AsnaValidation.DataCase

  alias AsnaValidation.Repo
  alias AsnaValidation.Templates

  describe "requirements" do
    alias AsnaValidation.Templates.Requirement

    @valid_attrs %{model: %{}, type: "some type"}
    @update_attrs %{model: %{}, type: "some updated type"}
    @invalid_attrs %{model: nil, type: nil}

    def requirement_fixture(attrs \\ %{}) do
      {:ok, requirement} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Templates.create_requirement()

      requirement
    end

    test "list_requirements/0 returns all requirements" do
      requirement = requirement_fixture()
      assert Templates.list_requirements() == [requirement]
    end

    test "get_requirement!/1 returns the requirement with given id" do
      requirement = requirement_fixture()
      assert Templates.get_requirement!(requirement.id) == requirement
    end

    # test "create_requirement/1 with valid data creates a requirement" do
    #   assert {:ok, %Requirement{} = requirement} = Templates.create_requirement(@valid_attrs)
    #   assert requirement.model == %{}
    #   assert requirement.type == "some type"
    # end

    # test "create_requirement/1 with invalid data returns error changeset" do
    #   assert {:error, %Ecto.Changeset{}} = Templates.create_requirement(@invalid_attrs)
    # end

    # test "update_requirement/2 with valid data updates the requirement" do
    #   requirement = requirement_fixture()
    #   assert {:ok, requirement} = Templates.update_requirement(requirement, @update_attrs)
    #   assert %Requirement{} = requirement
    #   assert requirement.model == %{}
    #   assert requirement.type == "some updated type"
    # end

    # test "update_requirement/2 with invalid data returns error changeset" do
    #   requirement = requirement_fixture()
    #   assert {:error, %Ecto.Changeset{}} = Templates.update_requirement(requirement, @invalid_attrs)
    #   assert requirement == Templates.get_requirement!(requirement.id)
    # end

    # test "delete_requirement/1 deletes the requirement" do
    #   requirement = requirement_fixture()
    #   assert {:ok, %Requirement{}} = Templates.delete_requirement(requirement)
    #   assert_raise Ecto.NoResultsError, fn -> Templates.get_requirement!(requirement.id) end
    # end

    # test "change_requirement/1 returns a requirement changeset" do
    #   requirement = requirement_fixture()
    #   assert %Ecto.Changeset{} = Templates.change_requirement(requirement)
    # end
  end

  describe "columns" do
    alias AsnaValidation.Templates.Column

    @valid_attrs %{active: true, name: "some name", optional: true, order: 0}
    @update_attrs %{active: false, name: "some updated name", optional: false, order: 1}
    @invalid_attrs %{active: nil, name: nil, optional: nil}

    def column_fixture(attrs \\ %{}) do
      {:ok, column} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Templates.create_column()

      Repo.preload(column, :requirements)
    end

    test "list_columns/0 returns all columns" do
      column = column_fixture()
      assert Templates.list_columns() == [column]
    end

    test "get_column!/1 returns the column with given id" do
      column = column_fixture()
      assert Templates.get_column!(column.id) == column
    end

    # test "create_column/1 with valid data creates a column" do
    #   assert {:ok, %Column{} = column} = Templates.create_column(@valid_attrs)
    #   assert column.active == true
    #   assert column.name == "some name"
    #   assert column.optional == true
    # end

    # test "create_column/1 with invalid data returns error changeset" do
    #   assert {:error, %Ecto.Changeset{}} = Templates.create_column(@invalid_attrs)
    # end

    # test "update_column/2 with valid data updates the column" do
    #   column = column_fixture()
    #   assert {:ok, column} = Templates.update_column(column, @update_attrs)
    #   assert %Column{} = column
    #   assert column.active == false
    #   assert column.name == "some updated name"
    #   assert column.optional == false
    # end

    # test "update_column/2 with invalid data returns error changeset" do
    #   column = column_fixture()
    #   assert {:error, %Ecto.Changeset{}} = Templates.update_column(column, @invalid_attrs)
    #   assert column == Templates.get_column!(column.id)
    # end

    # test "delete_column/1 deletes the column" do
    #   column = column_fixture()
    #   assert {:ok, %Column{}} = Templates.delete_column(column)
    #   assert_raise Ecto.NoResultsError, fn -> Templates.get_column!(column.id) end
    # end

    # test "change_column/1 returns a column changeset" do
    #   column = column_fixture()
    #   assert %Ecto.Changeset{} = Templates.change_column(column)
    # end
  end

  describe "templates" do
    alias AsnaValidation.Templates.Template

    @valid_attrs %{
      "active" => true,
      "name" => "some name",
      "type" => "some type",
      "columns" => [%{
        "name" => "some column name",
        "active" => true,
        "optional" => false,
        "order" => 0,
        "requirements" => [%{
          "type" => "columnType",
          "model" => %{
            "value" => "string"
          }
        }]
      }]
    }
    @update_attrs %{
      "active" => false,
      "name" => "some updated name",
      "type" => "some updated type",
      "columns" => [%{
        "name" => "some updated column name",
        "active" => true,
        "optional" => false,
        "order" => 1,
        "requirements" => [%{
          "type" => "length",
          "model" => %{
            "value" => "13"
          }
        }]
      }]
    }
    @invalid_attrs %{"active" => nil, "name" => nil, "type" => nil}

    def template_fixture(attrs \\ %{}) do
      {:ok, template} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Templates.create_template()

      Repo.preload(template, [columns: [:requirements]])
    end

    test "list_templates/0 returns all templates" do
      template = template_fixture()
      assert Templates.list_templates() == [template]
    end

    test "get_template!/1 returns the template with given id" do
      template = template_fixture()
      assert Templates.get_template!(template.id) == template
    end

    test "create_template/1 with valid data creates a template" do
      assert {:ok, %Template{} = template} = Templates.create_template(@valid_attrs)
      first_column = hd template.columns
      first_requirement = hd first_column.requirements

      assert template.active == true
      assert template.name == "some name"
      assert template.type == "some type"
      assert first_column.name == "some column name"
      assert first_column.order == 0
      assert first_requirement.type == "columnType"
    end

    test "create_template/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Templates.create_template(@invalid_attrs)
    end

    test "update_template/2 with valid data updates the template" do
      template = template_fixture()
      assert {:ok, template} = Templates.update_template(template, @update_attrs)
      first_column = hd template.columns
      first_requirement = hd first_column.requirements

      assert %Template{} = template
      assert template.active == false
      assert template.name == "some updated name"
      assert template.type == "some updated type"
      assert first_column.name == "some updated column name"
      assert first_column.order == 1
      assert first_requirement.type == "length"
    end

    test "update_template/2 with invalid data returns error changeset" do
      template = template_fixture()
      assert {:error, %Ecto.Changeset{}} = Templates.update_template(template, @invalid_attrs)
      assert template == Templates.get_template!(template.id)
    end

    test "delete_template/1 deletes the template" do
      template = template_fixture()
      assert {:ok, %Template{}} = Templates.delete_template(template)
      assert_raise Ecto.NoResultsError, fn -> Templates.get_template!(template.id) end
    end

    test "change_template/1 returns a template changeset" do
      template = template_fixture()
      assert %Ecto.Changeset{} = Templates.change_template(template)
    end
  end
end
