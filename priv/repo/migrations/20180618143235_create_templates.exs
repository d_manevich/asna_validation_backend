defmodule AsnaValidation.Repo.Migrations.CreateTemplates do
  use Ecto.Migration

  def change do
    create table(:templates) do
      add :name, :string
      add :type, :string
      add :active, :boolean, default: false, null: false

      timestamps()
    end

  end
end
