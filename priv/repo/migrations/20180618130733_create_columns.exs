defmodule AsnaValidation.Repo.Migrations.CreateColumns do
  use Ecto.Migration

  def change do
    create table(:columns) do
      add :name, :string
      add :order, :integer
      add :active, :boolean, default: false, null: false
      add :optional, :boolean, default: false, null: false

      timestamps()
    end

  end
end
