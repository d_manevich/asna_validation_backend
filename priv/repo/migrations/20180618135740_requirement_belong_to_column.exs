defmodule AsnaValidation.Repo.Migrations.RequirementBelongToColumn do
  use Ecto.Migration

  def change do
    alter table(:requirements) do
      add :column_id, references(:columns, on_delete: :delete_all)
    end
  end
end
