defmodule AsnaValidation.Repo.Migrations.CreateRequirements do
  use Ecto.Migration

  def change do
    create table(:requirements) do
      add :type, :string
      add :model, :map

      timestamps()
    end

  end
end
