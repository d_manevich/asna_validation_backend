defmodule AsnaValidation.Repo.Migrations.ColumnsBelongsToTemplate do
  use Ecto.Migration

  def change do
    alter table(:columns) do
      add :template_id, references(:templates, on_delete: :delete_all)
    end
  end
end
