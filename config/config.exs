# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :asna_validation,
  ecto_repos: [AsnaValidation.Repo]

# Configures the endpoint
config :asna_validation, AsnaValidationWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "+xLYME00yLQ7bI7Vflk1Yh8YcC67gHTiN+OVx1FupoL4ALRPo0l0cGqORzFSWQNC",
  render_errors: [view: AsnaValidationWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: AsnaValidation.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
