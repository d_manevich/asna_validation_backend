defmodule AsnaValidationWeb.TemplateView do
  use AsnaValidationWeb, :view
  alias AsnaValidationWeb.{TemplateView, ColumnView}

  def render("index.json", %{templates: templates}) do
    %{data: render_many(templates, TemplateView, "template.json")}
  end

  def render("show.json", %{template: template}) do
    %{data: render_one(template, TemplateView, "template.json")}
  end

  def render("template.json", %{template: template}) do

    %{
      id: template.id,
      name: template.name,
      type: template.type,
      active: template.active,
      columns: render_many(template.columns, ColumnView, "column.json")
    }
  end
end
