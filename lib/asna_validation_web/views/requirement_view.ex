defmodule AsnaValidationWeb.RequirementView do
  use AsnaValidationWeb, :view
  alias AsnaValidationWeb.RequirementView

  def render("index.json", %{requirements: requirements}) do
    %{data: render_many(requirements, RequirementView, "requirement.json")}
  end

  def render("show.json", %{requirement: requirement}) do
    %{data: render_one(requirement, RequirementView, "requirement.json")}
  end

  def render("requirement.json", %{requirement: requirement}) do
    %{id: requirement.id,
      type: requirement.type,
      model: requirement.model}
  end
end
