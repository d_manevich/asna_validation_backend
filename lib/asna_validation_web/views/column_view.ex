defmodule AsnaValidationWeb.ColumnView do
  use AsnaValidationWeb, :view
  alias AsnaValidationWeb.{ColumnView, RequirementView}

  def render("index.json", %{columns: columns}) do
    %{data: render_many(columns, ColumnView, "column.json")}
  end

  def render("show.json", %{column: column}) do
    %{data: render_one(column, ColumnView, "column.json")}
  end

  def render("column.json", %{column: column}) do
    %{id: column.id,
      name: column.name,
      active: column.active,
      optional: column.optional,
      order: column.order,
      requirements: render_many(column.requirements, RequirementView, "requirement.json")}
  end
end
