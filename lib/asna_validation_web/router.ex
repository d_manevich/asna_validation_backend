defmodule AsnaValidationWeb.Router do
  use AsnaValidationWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", AsnaValidationWeb do
    pipe_through :api
    
    resources "/requirements", RequirementController, only: [:index, :show]
    resources "/columns", ColumnController, only: [:index, :show]
    resources "/templates", TemplateController, except: [:new, :edit]
  end
end
