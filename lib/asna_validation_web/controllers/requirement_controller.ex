defmodule AsnaValidationWeb.RequirementController do
  use AsnaValidationWeb, :controller

  alias AsnaValidation.Templates
  alias AsnaValidation.Templates.Requirement

  action_fallback AsnaValidationWeb.FallbackController

  def index(conn, _params) do
    requirements = Templates.list_requirements()
    render(conn, "index.json", requirements: requirements)
  end

  # def create(conn, %{"requirement" => requirement_params}) do
  #   with {:ok, %Requirement{} = requirement} <- Templates.create_requirement(requirement_params) do
  #     conn
  #     |> put_status(:created)
  #     |> put_resp_header("location", requirement_path(conn, :show, requirement))
  #     |> render("show.json", requirement: requirement)
  #   end
  # end

  def show(conn, %{"id" => id}) do
    requirement = Templates.get_requirement!(id)
    render(conn, "show.json", requirement: requirement)
  end

  # def update(conn, %{"id" => id, "requirement" => requirement_params}) do
  #   requirement = Templates.get_requirement!(id)

  #   with {:ok, %Requirement{} = requirement} <- Templates.update_requirement(requirement, requirement_params) do
  #     render(conn, "show.json", requirement: requirement)
  #   end
  # end

  # def delete(conn, %{"id" => id}) do
  #   requirement = Templates.get_requirement!(id)
  #   with {:ok, %Requirement{}} <- Templates.delete_requirement(requirement) do
  #     send_resp(conn, :no_content, "")
  #   end
  # end
end
