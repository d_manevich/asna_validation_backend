defmodule AsnaValidationWeb.ColumnController do
  use AsnaValidationWeb, :controller

  alias AsnaValidation.Templates
  alias AsnaValidation.Templates.Column

  action_fallback AsnaValidationWeb.FallbackController

  def index(conn, _params) do
    columns = Templates.list_columns()
    render(conn, "index.json", columns: columns)
  end

  # def create(conn, %{"column" => column_params}) do
  #   with {:ok, %Column{} = column} <- Templates.create_column(column_params) do
  #     conn
  #     |> put_status(:created)
  #     |> put_resp_header("location", column_path(conn, :show, column))
  #     |> render("show.json", column: column)
  #   end
  # end

  def show(conn, %{"id" => id}) do
    column = Templates.get_column!(id)
    render(conn, "show.json", column: column)
  end

  # def update(conn, %{"id" => id, "column" => column_params}) do
  #   column = Templates.get_column!(id)

  #   with {:ok, %Column{} = column} <- Templates.update_column(column, column_params) do
  #     render(conn, "show.json", column: column)
  #   end
  # end

  # def delete(conn, %{"id" => id}) do
  #   column = Templates.get_column!(id)
  #   with {:ok, %Column{}} <- Templates.delete_column(column) do
  #     send_resp(conn, :no_content, "")
  #   end
  # end
end
