defmodule AsnaValidation.Templates do
  @moduledoc """
  The Templates context.
  """

  import Ecto.Query, warn: false
  alias AsnaValidation.Repo

  alias AsnaValidation.Templates.Requirement

  @doc """
  Returns the list of requirements.

  ## Examples

      iex> list_requirements()
      [%Requirement{}, ...]

  """
  def list_requirements do
    Repo.all(Requirement)
  end

  @doc """
  Gets a single requirement.

  Raises `Ecto.NoResultsError` if the Requirement does not exist.

  ## Examples

      iex> get_requirement!(123)
      %Requirement{}

      iex> get_requirement!(456)
      ** (Ecto.NoResultsError)

  """
  def get_requirement!(id), do: Repo.get!(Requirement, id)

  @doc """
  Creates a requirement.

  ## Examples

      iex> create_requirement(%{field: value})
      {:ok, %Requirement{}}

      iex> create_requirement(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_requirement(attrs \\ %{}) do
    %Requirement{}
    |> Requirement.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a requirement.

  ## Examples

      iex> update_requirement(requirement, %{field: new_value})
      {:ok, %Requirement{}}

      iex> update_requirement(requirement, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_requirement(%Requirement{} = requirement, attrs) do
    requirement
    |> Requirement.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Requirement.

  ## Examples

      iex> delete_requirement(requirement)
      {:ok, %Requirement{}}

      iex> delete_requirement(requirement)
      {:error, %Ecto.Changeset{}}

  """
  def delete_requirement(%Requirement{} = requirement) do
    Repo.delete(requirement)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking requirement changes.

  ## Examples

      iex> change_requirement(requirement)
      %Ecto.Changeset{source: %Requirement{}}

  """
  def change_requirement(%Requirement{} = requirement) do
    Requirement.changeset(requirement, %{})
  end

  alias AsnaValidation.Templates.Column

  @doc """
  Returns the list of columns.

  ## Examples

      iex> list_columns()
      [%Column{}, ...]

  """
  def list_columns do
    Repo.all(Column) |> Repo.preload(:requirements)
  end

  @doc """
  Gets a single column.

  Raises `Ecto.NoResultsError` if the Column does not exist.

  ## Examples

      iex> get_column!(123)
      %Column{}

      iex> get_column!(456)
      ** (Ecto.NoResultsError)

  """
  def get_column!(id), do: Repo.get!(Column, id) |> Repo.preload(:requirements)

  @doc """
  Creates a column.

  ## Examples

      iex> create_column(%{field: value})
      {:ok, %Column{}}

      iex> create_column(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_column(attrs \\ %{}) do
    %Column{}
    |> Column.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a column.

  ## Examples

      iex> update_column(column, %{field: new_value})
      {:ok, %Column{}}

      iex> update_column(column, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_column(%Column{} = column, attrs) do
    column
    |> Column.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Column.

  ## Examples

      iex> delete_column(column)
      {:ok, %Column{}}

      iex> delete_column(column)
      {:error, %Ecto.Changeset{}}

  """
  def delete_column(%Column{} = column) do
    Repo.delete(column)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking column changes.

  ## Examples

      iex> change_column(column)
      %Ecto.Changeset{source: %Column{}}

  """
  def change_column(%Column{} = column) do
    Column.changeset(column, %{})
  end

  alias AsnaValidation.Templates.Template

  @doc """
  Returns the list of templates.

  ## Examples

      iex> list_templates()
      [%Template{}, ...]

  """
  def list_templates do
    query = from c in Column, order_by: c.order
    Repo.all(Template) |> Repo.preload([columns: {query, [:requirements]}])
  end

  @doc """
  Gets a single template.

  Raises `Ecto.NoResultsError` if the Template does not exist.

  ## Examples

      iex> get_template!(123)
      %Template{}

      iex> get_template!(456)
      ** (Ecto.NoResultsError)

  """
  def get_template!(id) do
    query = from c in Column, order_by: c.order
    Repo.get!(Template, id) |> Repo.preload([columns: {query, [:requirements]}])
  end

  @doc """
  Creates a template.

  ## Examples

      iex> create_template(%{field: value})
      {:ok, %Template{}}

      iex> create_template(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_template(attrs \\ %{}) do
    %Template{}
    |> Template.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a template.

  ## Examples

      iex> update_template(template, %{field: new_value})
      {:ok, %Template{}}

      iex> update_template(template, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_template(%Template{} = template, attrs) do
    id = template.id
    active = attrs["active"]
    type = attrs["type"] || template.type


    if active do
      active_query = from t in Template, where: t.id != ^id and t.type == ^type and t.active
      Repo.update_all(active_query, set: [active: false])
    end

    template
    |> Template.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Template.

  ## Examples

      iex> delete_template(template)
      {:ok, %Template{}}

      iex> delete_template(template)
      {:error, %Ecto.Changeset{}}

  """
  def delete_template(%Template{} = template) do
    Repo.delete(template)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking template changes.

  ## Examples

      iex> change_template(template)
      %Ecto.Changeset{source: %Template{}}

  """
  def change_template(%Template{} = template) do
    Template.changeset(template, %{})
  end
end
