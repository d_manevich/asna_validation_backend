defmodule AsnaValidation.Templates.Template do
  use Ecto.Schema
  import Ecto.Changeset
  alias AsnaValidation.Templates.Column


  schema "templates" do
    field :active, :boolean, default: false
    field :name, :string
    field :type, :string
    has_many :columns, Column, [on_delete: :delete_all, on_replace: :delete]

    timestamps()
  end

  @doc false
  def changeset(template, attrs) do
    template
    |> cast(attrs, [:name, :type, :active])
    |> cast_assoc(:columns)
    |> validate_required([:name, :type, :active])
  end
end
