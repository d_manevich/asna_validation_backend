defmodule AsnaValidation.Templates.Requirement do
  use Ecto.Schema
  import Ecto.Changeset
  alias AsnaValidation.Templates.Column


  schema "requirements" do
    field :model, :map
    field :type, :string
    belongs_to :column, Column, on_replace: :delete

    timestamps()
  end

  @doc false
  def changeset(requirement, attrs) do
    requirement
    |> cast(attrs, [:type, :model])
    |> validate_required([:type, :model])
  end
end
