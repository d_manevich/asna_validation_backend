defmodule AsnaValidation.Templates.Column do
  use Ecto.Schema
  import Ecto.Changeset
  alias AsnaValidation.Templates.Requirement
  alias AsnaValidation.Templates.Template


  schema "columns" do
    field :active, :boolean, default: false
    field :name, :string
    field :order, :integer
    field :optional, :boolean, default: false
    has_many :requirements, Requirement, [on_delete: :delete_all, on_replace: :delete]
    belongs_to :template, Template, on_replace: :delete

    timestamps()
  end

  @doc false
  def changeset(column, attrs) do
    column
    |> cast(attrs, [:name, :active, :optional, :order])
    |> cast_assoc(:requirements)
    |> validate_required([:name, :active, :optional, :order])
  end
end
